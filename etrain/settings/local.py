import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ccz=5oa=1y$24sh341*z9gv8t21bf+((6q1#pa8br^u3ase-o%'

DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1','https://etraindemo.herokuapp.com/','etraindemo.herokuapp.com/','www.etraindemo.herokuapp.com/']

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}



STATIC_ROOT=os.path.join(BASE_DIR,'static-cdn')
STATICFILES_DIRS=[os.path.join(BASE_DIR,'static')]
