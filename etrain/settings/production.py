import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

SECRET_KEY = 'ccz=5oaasd=1y$24sh341*z9gv8t21bf+(erygb54t5ge*8br^u3ase-o%'

DEBUG = False

ALLOWED_HOSTS = ['142.93.103.234','localhost','www.elektronmaktab.uz','elektronmaktab.uz ']


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'elektronmaktab.uz',
        'USER':'myusername',
        'PASSWORD':'mypassword',
        'HOST':'localhost',
        'PORT':'5432',
    }
}

STATIC_ROOT=os.path.join(BASE_DIR,'static-cdn')
STATICFILES_DIRS=[os.path.join(BASE_DIR,'static')]
