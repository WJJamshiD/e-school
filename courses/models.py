from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.shortcuts import reverse
from django.urls import URLResolver
from django.utils.safestring import mark_safe
from .custom_fields import OrderField
from django.template.loader import render_to_string
from django.db.models.signals import pre_save
from django.utils.text import slugify 
from django.dispatch import receiver

User=get_user_model()



def create_myslug(instan,new_slug=None):
	slug=slugify(instan.title)
	if new_slug is not None:
		slug=new_slug
	qs=instan.__class__.objects.filter(slug=slug).order_by("-id")
	try:
		qs=qs.exclude(pk=instan.pk)
	except:
		pass
	if qs.count() > 0:
		print(qs.count())
		new_slug='{}-{}'.format(slug,qs.first().id)
		return create_myslug(instan,new_slug=new_slug)
	return slug



# Create your models here.
class Subject(models.Model):
	title=models.CharField('Fan nomi',max_length=240,)
	description=models.TextField("Qisqacha ta'rif",max_length=3000,blank=True,null=True)
	slug =models.SlugField(unique=True)
	created=models.DateTimeField('Yaratilgan vaqti',auto_now=False, auto_now_add=True)

	class Meta:
		verbose_name = "Fan"
		verbose_name_plural ="Fanlar"
		ordering=['title']

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse("subject_detail", kwargs={"subject": self.slug})

	
@receiver(pre_save, sender=Subject)
def subject_pre_save_receiver(sender,instance, **kwargs):
	instance.slug=create_myslug(instance)    

def upload_path_course(instan,file):
	return 'courses/{0}/{1}'.format(slugify(instan.title),file)

class Course(models.Model):
	owner=models.ForeignKey(User,verbose_name="O'qituvchi",related_name='owned_courses', on_delete=models.CASCADE)
	students = models.ManyToManyField(User,related_name='courses_joined',blank=True)
	subject=models.ForeignKey(Subject,verbose_name="Fan",on_delete=models.CASCADE)
	title=models.CharField('Kurs nomi', max_length=250)
	overview=models.TextField("Qisqacha ma'lumot", max_length=3000,null=True,blank=True)
	created=models.DateTimeField("Yaratilgan vaqti", auto_now=False, auto_now_add=True)
	updated=models.DateTimeField('Yangilandi', auto_now=True, auto_now_add=False)
	# price =models.PositiveIntegerField(default=0)
	banner=models.ImageField(upload_to=upload_path_course)
	slug=models.SlugField(unique=True,max_length=200)
	video=models.FileField(upload_to=upload_path_course,null=True,blank=True)
	about=models.TextField()

	class Meta:
		verbose_name ="Kurs"
		verbose_name_plural ="Kurslar"
		ordering=['-created']

	def __str__(self):
		return self.title

	def delete(self,*args, **kwargs):
		self.banner.delete()
		self.video.delete()
		super().delete(*args, **kwargs)

	def get_absolute_url(self):
		return reverse("course_detail", kwargs={"slug": self.slug})

	def get_update_url(self):
		return reverse('course_edit', kwargs={"slug": self.slug})

	def get_delete_url(self):
		return reverse('course_delete', kwargs={"slug": self.slug})

	def get_add_section_url(self):
		return reverse('section_create', kwargs={"slug": self.slug})

	def get_add_student_url(self):
		return reverse('add_student_to_course', kwargs={"slug": self.slug})

	def get_student_list_url(self):
		return f'/course/student/mine#{self.id}'

	def get_start_url(self):
		return self.section.first().lesson.first().get_absolute_url()

	@property
	def lessons(self):
		lessons=Lesson.objects.filter(section__course=self.id)
		return lessons.count()

@receiver(pre_save, sender=Course)
def course_pre_save_receiver(sender,instance, **kwargs):
		instance.slug=create_myslug(instance)


class Section(models.Model):
	course=models.ForeignKey(Course, verbose_name="Kurs", on_delete=models.CASCADE,related_name='section')
	title=models.CharField("Bo'lim nomi", max_length=250)
	order=OrderField(blank=True,for_fields=['course'])
	created=models.DateTimeField( auto_now=False, auto_now_add=True)
	updated=models.DateTimeField(auto_now=True, auto_now_add=False)

	class Meta:
		verbose_name = "Bo'lim"
		verbose_name_plural = "Bo'limlar"
		ordering=['order','course']

	def __str__(self):
		return f'{self.order}. {self.title}'

	def get_absolute_url(self):
		return reverse("section_detail", kwargs={"section_id": self.pk})

	def get_update_url(self):
		return reverse('section_update',kwargs={"section_id": self.pk,"slug":self.course.slug})

	def get_delete_url(self):
		return reverse('section_delete',kwargs={"section_id": self.pk,"slug":self.course.slug})
	
	def get_add_lesson_url(self):
		return reverse('lesson_create',kwargs={"section_id": self.pk})

	def changeform_link(self):
		if self.id:
			changeform_url=reverse('admin:courses_section_change',args=(self.id,))
			return mark_safe("<a href='{}' target='_blank'><b>Shu bo'limga darslarni qo'shish</b></a>".format(changeform_url))
		return ''
	changeform_link.short_description=''

  
	def owner_name(self):
		return self.course.owner
	owner_name.short_description='Avtor nomi'

	author_name=property(owner_name)

def upload_path_lesson(instan,file):
	section=instan.section
	course=slugify(section.course)
	return f'courses/{slugify(section)}/{slugify(instan)}/{file}'

class Lesson(models.Model):
	section=models.ForeignKey(Section, on_delete=models.CASCADE,related_name='lesson')
	title=models.CharField("Sarlavha", max_length=200)
	slug=models.SlugField(unique=True)
	order=OrderField(blank=True,for_fields=['section'])
	lecture=models.TextField(blank=True,null=True)
	video=models.FileField(upload_to=upload_path_lesson)
	open=models.BooleanField(default=False)

	class Meta:
		verbose_name = "Dars"
		verbose_name_plural ="Darslar"
		ordering=['order','section']

	def __str__(self):
		return f"{self.order}. {self.title}"

	def delete(self,*args, **kwargs):
		self.video.delete()
		super().delete(*args, **kwargs)

	def get_absolute_url(self):
		return reverse("lesson_detail", kwargs={"lesson": self.slug})

	def get_update_url(self):
		return reverse("lesson_update", kwargs={"lesson": self.slug,'section_id':self.section.pk})
	
	def get_delete_url(self):
		return reverse("lesson_delete", kwargs={"lesson": self.slug})
	
	def get_add_test_url(self):
		return reverse("lesson_test_create", kwargs={"lesson": self.slug})


	@property
	def kurs_nomi(self):
		return self.section.course

	def owner(self):
		return self.section.course.owner

	def next(self):
		order=self.order+1
		next_les=Lesson.objects.filter(section=self.section,order=order)
		if next_les.exists():
			return next_les.first()

@receiver(pre_save, sender=Lesson)
def lesson_pre_save_receiver(sender,instance, **kwargs):
	instance.slug=create_myslug(instance)    

	
class Content(models.Model):
	lesson=models.ForeignKey(Lesson, verbose_name="Material", on_delete=models.CASCADE,related_name='materiallar',null=True,blank=True)
	content_type=models.ForeignKey(ContentType, verbose_name="Material turi", on_delete=models.CASCADE, 
	limit_choices_to={'model__in':( 'video',
									'file',
									'image')})

	object_id=models.PositiveSmallIntegerField()
	item=GenericForeignKey('content_type','object_id')


	class Meta:
		verbose_name = " Dars materiali"
		verbose_name_plural = " Dars materiallari"

	def __str__(self):
		return self.item.title

	def get_absolute_url(self):
		return reverse("content_detail", kwargs={"pk": self.pk})

	def get_delete_url(self):
		return reverse("content_delete", kwargs={"id": self.pk})

class ItemBase(models.Model):
	owner=models.ForeignKey(User,verbose_name="Avtor",related_name='owned_%(class)ss', on_delete=models.CASCADE)    
	title = models.CharField(max_length=100)    
	created = models.DateTimeField('Yaratilgan',auto_now_add=True)
	updated=models.DateTimeField('Yangilangan', auto_now=True, auto_now_add=False)
		
	class Meta:        
		abstract = True
	
	def  __str__(self):
		return self.title
	
	def render(self):
		return render_to_string(
			f'courses/content/{self._meta.model_name}.html',
			{'item': self})

class Text(ItemBase):    
	content = models.TextField()


class File(ItemBase):
	file=models.FileField(upload_to='lesson_files/files')

	def delete(self,*args, **kwargs):
		self.file.delete()
		super().delete(*args, **kwargs)

class Image(ItemBase):
	file=models.FileField( upload_to='lesson_files/images')

	def delete(self,*args, **kwargs):
		self.file.delete()
		super().delete(*args, **kwargs)

class Video(ItemBase):
	url=models.URLField()



# 'mkdir courses/fixtures'
# 'python manage.py dumpdata courses --indent=2 --output=courses/fixtures/Matematika.json' 
# to get data in json format
# type  'python manage.py loaddata Matematika.json'
# to load this initial data from *fixtures folders



class Test(models.Model):
	owner=models.ForeignKey(User, on_delete=models.CASCADE)
	subject=models.ForeignKey(Subject,on_delete=models.SET_NULL,null=True)
	lesson=models.ManyToManyField(Lesson,related_name='tests',null=True,blank=True)
	title=models.CharField(max_length=150)
	test_file=models.FileField(upload_to='tests', max_length=100)
	test_number=models.PositiveSmallIntegerField()
	test_answer=models.CharField(max_length=200)
	created=models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering=['-created']
	

	def __str__(self):
		return self.title

	def delete(self,*args, **kwargs):
		self.test_file.delete()
		super().delete(*args, **kwargs)

	def get_results_url(self):
		return reverse('test_results',kwargs={'id':self.id})

	def get_absolute_url(self):
		return reverse('test_solve', kwargs={'id':self.id})
	
	def get_delete_url(self):
		return reverse('test_delete', kwargs={'id':self.id})

	@property
	def percentage(self):
		results=self.results.all()
		if results.count() > 0:
			all=sum(x.percentage for x in results )
			return int(all/results.count())
		return 0
	
	@property
	def mean_value(self):
		results=self.results.all()
		if results.count() > 0:
			all=sum([x.result for x in results ])
			return int(all/results.count())
		return 0
	
	@property
	def full(self):
		results=self.results.all()
		if results.count() > 0:
			results=[x.result for x in results ]
			return results.count(self.test_number)
		return 0
	
	@property
	def full_per(self):
		results=self.results.all()
		if results.count() > 0:
			results=[x.result for x in results ]
			return int(100*results.count(self.test_number)/len(results))
		return 0
	


class TestResult(models.Model):
	test=models.ForeignKey(Test,on_delete=models.CASCADE,related_name='results')
	student=models.ForeignKey(User,on_delete=models.CASCADE,related_name='test_results')
	answer=models.CharField(max_length=400)
	result=models.PositiveSmallIntegerField()
	created=models.DateTimeField(auto_now=False, auto_now_add=True)

	class Meta:
		ordering=['-result']
	
	
	def __str__(self):
		return f'{self.student} : {self.test}'
	

	@property
	def percentage(self):
		return int(100*self.result/self.test.test_number)
	
	@property
	def rating(self):
		test=self.test
		results=[x.result for x in test.results.all() ]
		all=len(set(results))
		results=list(set(results))
		rating=all-results.index(self.result)
		return rating

	
@receiver(pre_save, sender=TestResult)
def testresult_pre_save_receiver(sender,instance,*args, **kwargs):
	answer=instance.answer.lower()
	keys=instance.test.test_answer
	test_number=instance.test.test_number
	count=0
	try:
		for i in range(test_number):
			if answer[i]==keys[i]:
				count+=1
	except:
		pass
	instance.result=count


# pre_save.connect(testresult_pre_save_receiver,TestResult)
