from django import template
from courses.models import Course
register = template.Library()

@register.filter
def model_name(obj):
    try:
        return obj._meta.model_name
    except AttributeError:
        return None

@register.filter
def get_course_sections(value):
    try:
        obj=Course.objects.get(id=int(value))
        return obj.section.all()
    except Exception as e:
        print(e)
        return 1