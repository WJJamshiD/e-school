from django.urls import path
from .views import ManageCourseListView, CourseCreateView, CourseUpdateView, \
    CourseDeleteView,CourseSectionUpdateView,ContentCreateView,ContentDeleteView, \
    LessonDetailView
from . import views

urlpatterns = [
    path('all/', views.CourseListView.as_view(), name='all_courses'),
    path('mine/', ManageCourseListView.as_view(),name='manage_course_list'),
    path('create/', CourseCreateView.as_view(),name='course_create'),
    path('<slug>/edit/',CourseUpdateView.as_view(),name='course_edit'),
    path('<slug>/delete/',CourseDeleteView.as_view(),name='course_delete'),
    path('<slug>/section/create',CourseSectionUpdateView.as_view(),name='section_create'),
    path('<slug>/student/add',views.CourseAddStudentView.as_view(),name='add_student_to_course'),
    path('<slug>/student/<int:student_id>/delete',views.CourseStudentDeleteView.as_view(),name='delete_student_from_course'),
    path('section/create',CourseSectionUpdateView.as_view(),name='section_create_optional'),
    path('<slug>/section/<int:section_id>/edit',CourseSectionUpdateView.as_view(),name='section_update'),
    path('<slug>/section/<int:section_id>/delete',views.CourseSectionDeleteView.as_view(),name='section_delete'),
    path('lesson/<lesson>/content/<model_name>/create/', ContentCreateView.as_view(),name='content_create'),
    path('section/<int:section_id>/lesson/create', views.LessonCreateUpdateView.as_view(),name='lesson_create'),
    path('section/<int:section_id>/lesson/<lesson>', views.LessonCreateUpdateView.as_view(),name='lesson_update'),
    path('content/<int:id>/delete', ContentDeleteView.as_view(),name='content_delete'),
    # path('section/<int:section_id>/',SectionContentListView.as_view(),name='section_detail'),
    path('test/create',views.TestCreateView.as_view(),name='test_create'),
    path('test/<int:id>',views.TestResultView.as_view(),name='test_results'),
    path('test/<int:id>/delete',views.TestDeleteView.as_view(),name='test_delete'),
    path('test/<int:id>/solve',views.TestSolveView.as_view(),name='test_solve'),
    path('subject/<slug:subject>/',views.CourseListView.as_view(),name='course_list_subject'),
    path('<slug:slug>/', views.CourseDetailView.as_view(), name='course_detail'),
    path('lesson/<slug:lesson>/', views.LessonDetailView.as_view(), name='lesson_detail'),
    path('lesson/<slug:lesson>/test/create', views.TestCreateView.as_view(), name='lesson_test_create'),
    path('lesson/create', views.LessonCreateUpdateView.as_view(), name='lesson_create_optional'),
    path('lesson/<slug:lesson>/delete', views.LessonDeleteView.as_view(), name='lesson_delete'),
    path('ajax/get_course_sections',views.get_course_sections, name='get_course_sections'),
    path('test/mine',views.TestListView.as_view(), name='test_list'),
    path('<slug:subject>/courses',views.CourseListView.as_view(), name='subject_detail'),
    path('student/mine',views.MyStudentView.as_view(), name='mystudents'),  
]
