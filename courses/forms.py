from django import forms
from django.forms.models import inlineformset_factory
from .models import Course, Section,Lesson

class SectionUpdateForm(forms.ModelForm):
    class Meta:
        model=Section
        fields=['title','course']


class LessonUpdateForm(forms.ModelForm):
    class Meta:
        model=Lesson
        fields=['title','section','lecture','video']