from django.shortcuts import render,redirect,get_object_or_404,Http404
from .mixins import OwnerCourseEditMixin,OwnerCourseMixin,OwnerMixin
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.views.generic import ListView,DetailView
from django.views.generic.base import TemplateResponseMixin, View,TemplateView
from .forms import SectionUpdateForm,LessonUpdateForm
from .models import Course,Section,Content,Lesson,Subject,TestResult,Test
from django.apps import apps
from django.forms.models import modelform_factory
from django.db.models import Count
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.http.response import JsonResponse
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
# Create your views here.
	
User=get_user_model()

class CourseListView(TemplateResponseMixin, View):
	model = Course
	template_name = 'new/course_list.html'
	
	def get(self, request, subject=None):
		courses = Course.objects.annotate(total_sections=Count('section'))
		if subject:
			subject = get_object_or_404(Subject, slug=subject)
			courses = courses.filter(subject=subject)
		return self.render_to_response({'subject': subject,
										'courses': courses,
										})
									
class CourseDetailView(DetailView):
	model = Course
	template_name = 'new/tutor_course_detail.html'


class ManageCourseListView(OwnerCourseMixin, ListView):
	template_name = 'new/tutor_courses.html'
	permission_required = 'courses.view_course'

class CourseCreateView(OwnerCourseEditMixin, CreateView):
	permission_required = 'courses.add_course'

	def post(self,request,*args, **kwargs):
		print(self.get_form().errors)
		return super().post(request,*args, **kwargs)

	
class CourseUpdateView(OwnerCourseEditMixin, UpdateView):
	permission_required = 'courses.change_course'

	
class CourseDeleteView(OwnerCourseMixin, DeleteView):
	template_name = 'new/course_delete.html'
	permission_required = 'courses.delete_course'

class CourseSectionUpdateView(TemplateResponseMixin, View):
	template_name = 'new/section_update.html'
	course = None
	section=None
   
	def dispatch(self, request, slug=None,section_id=None, *args,**kwargs):
		if slug:
			self.course = get_object_or_404(Course, slug=slug, owner=request.user)
		if section_id:
			self.section=get_object_or_404(Section,id=section_id,course=self.course)
		return super().dispatch(request, slug,section_id,*args, **kwargs)
   
	def get(self, request,slug=None,section_id=None, *args, **kwargs):
		if Course.objects.count() == 0:
			return self.render_to_response({'empty':True})
		form = SectionUpdateForm(instance=self.section)
		return self.render_to_response({'course': self.course, 'form': form,'section':self.section})
   
	def post(self, request,slug=None,section_id=None, *args, **kwargs):
		form = SectionUpdateForm(instance=self.section,data=request.POST)
		if form.is_valid():
			obj=form.save(commit=False)
			obj.save()
			return redirect(obj.course.get_absolute_url())
		return self.render_to_response({'course': self.course, 'form': form})


class CourseSectionDeleteView(View):
	def get(self,request,slug,section_id):
		course=get_object_or_404(Course,slug=slug)
		section=get_object_or_404(Section,id=section_id,course=course,course__owner=request.user)
		section.delete()
		return redirect(course.get_absolute_url()) 

class LessonDeleteView(View):
	def get(self,request,lesson):
		lesson=get_object_or_404(Lesson,slug=lesson,section__course__owner=request.user)
		course=lesson.section.course
		lesson.delete()
		return redirect(course.get_absolute_url()) 

class ContentCreateView(TemplateResponseMixin, View):
	lesson = None
	model = None
	obj = None
	template_name = 'new/content_create.html'
	
	def get_model(self, model_name):
		if model_name in ['video', 'image', 'file']:
			return apps.get_model(app_label='courses', model_name=model_name)
		return None
	
	def get_form(self, model, *args, **kwargs):
		Form = modelform_factory(model, exclude=['owner',
												'created',
												'updated'])
		return Form(*args, **kwargs)
	
	def dispatch(self, request, lesson, model_name):
		self.lesson = get_object_or_404(Lesson, slug=lesson, section__course__owner=request.user)
		self.model = self.get_model(model_name)
		return super().dispatch(request, lesson, model_name)

	def get(self, request, lesson, model_name):
		form = self.get_form(self.model)
		return self.render_to_response({'form': form})
	
	def post(self, request, lesson, model_name):
		form = self.get_form(self.model,
							data=request.POST,
							files=request.FILES)
		if form.is_valid():
			obj = form.save(commit=False)
			obj.owner = request.user
			obj.save()
			Content.objects.create(lesson=self.lesson, item=obj)
			return redirect('lesson_update', self.lesson.section.id,self.lesson.slug)
		return self.render_to_response({'form': form})


class LessonCreateUpdateView(TemplateResponseMixin, View):
	template_name = 'new/tutor_lesson_update.html'
	section=None
	lesson=None
	
	def dispatch(self, request, section_id=None,lesson=None,*args, **kwargs):
		if section_id:
			self.section = get_object_or_404(Section, id=section_id, course__owner=request.user)
		if lesson:
			self.lesson=get_object_or_404(Lesson,slug=lesson)

		return super().dispatch(request, section_id,lesson,*args, **kwargs)

	def get(self, request,section_id=None,lesson=None,*args, **kwargs):
		if Section.objects.filter(course__owner=request.user).count() == 0:
			return self.render_to_response({'empty':True})
		form=LessonUpdateForm(instance=self.lesson)
		sections=''
		if self.lesson:
			sections=Section.objects.filter(course__owner=request.user,course=self.section.course)
		courses=Course.objects.filter(owner=request.user)

		return self.render_to_response({'section': self.section,'lesson':self.lesson,
										'form':form,'sections':sections,'courses':courses})
	
	def post(self, request,section_id=None,lesson=None,*args, **kwargs):
		courses=Course.objects.filter(owner=request.user)
		sections=''
		if self.section:
			sections=Section.objects.filter(course__owner=request.user,course=self.section.course)
		form = LessonUpdateForm(instance=self.lesson,data=request.POST,	files=request.FILES)
		print(form)
		if form.is_valid():
			data=form.cleaned_data
			print(data)
			obj = form.save(commit=False)
			obj.save()
			files=obj.materiallar.all()
			data=request.POST
			print(data)

			for file in files:
				file.item.title=data[f"{file.content_type}-{file.id}"]
				print(data[f"{file.content_type}-{file.id}"])
				file.item.save()
			
			tests=obj.tests.all()
			for test in tests:
				test.title=data[f"test-{test.id}"]
				test.save()
			return redirect(obj.section.course.get_absolute_url())
		else:
			print(form.errors)
		return self.render_to_response({'form': form,'lesson': self.lesson,'courses':courses,
										'sections':sections,'section':self.section})

class ContentDeleteView(View):
	def get(self, request, id):
		content = get_object_or_404(Content, id=id, lesson__section__course__owner=request.user)
		lesson=content.lesson
		section=lesson.section
		content.item.delete()
		content.delete()
		return redirect('lesson_update', section.id, lesson.slug)


class LessonDetailView(TemplateResponseMixin, View,LoginRequiredMixin):
	template_name = 'new/lesson.html'

	def get(self, request,lesson): 
		lesson = get_object_or_404(Lesson,slug=lesson)
		if (not lesson.open) and (request.user != lesson.section.course.owner) and (request.user not in lesson.section.course.students.all()):
			raise Http404
		tests=Test.objects.filter(lesson=lesson)
		return self.render_to_response({'lesson': lesson,'tests':tests})
		



#section/<int:section_id>/test/<int:id>
class TestCreateView(TemplateResponseMixin,View,PermissionRequiredMixin):
	permission_required='courses.add_test'
	template_name = "new/test_create.html"
	lesson=None
	
	def get_form(self,*args, **kwargs):
		Form = modelform_factory(Test, exclude=['owner','created'])
		return Form(*args, **kwargs)

	def dispatch(self, request, lesson=None, *args,**kwargs):
		if lesson:
			self.lesson = get_object_or_404(Lesson, slug=lesson)
		return super().dispatch(request, lesson,*args, **kwargs)

	def get(self, request,lesson=None):	
		lessons=Lesson.objects.filter(section__course__owner=request.user)
		form = self.get_form()
		return self.render_to_response({'form': form,'lesson':self.lesson,'lessons':lessons})
	
	def post(self, request,lesson=None):
		lessons=Lesson.objects.filter(section__course__owner=request.user)
		form = self.get_form(data=request.POST,	files=request.FILES)
		print(form)
		if form.is_valid():
			obj = form.save(commit=False)
			obj.owner = request.user
			obj.save()
			for lesson in form.cleaned_data['lesson']:
				print(lesson)
				obj.lesson.add(lesson)
			return redirect('test_list')
		print(form.errors)
		return self.render_to_response({'form': form,'lesson':self.lesson,'lessons':lessons})


class TestDeleteView(View):
	def get(self,request,id):
		test=get_object_or_404(Test,owner=request.user,id=id)
		test.delete()
		return redirect('manage_course_list') 


class TestResultView(TemplateView):
	template_name = "new/test_results.html"

	def get(self, request,id):
		test=get_object_or_404(Test,id=id)
		results=test.results.all()
		return self.render_to_response({'test':test,'results':results})



class TestSolveView(TemplateResponseMixin,View):
	template_name = "new/test_solve.html"

	def get(self, request,id):
		test=get_object_or_404(Test,id=id)
		student_solved=test.results.filter(student=request.user)
		if student_solved:
			student_last_solved=student_solved.last()		
			result=student_last_solved.result
			return self.render_to_response({'test': test,'result':result})
		return self.render_to_response({'test': test,'range':range(test.test_number)})
	
	def post(self, request,id):
		test=get_object_or_404(Test,id=id)
		print(test)
		obj = TestResult(test=test,student=request.user)
		print(obj)
		answer_check=dict(request.POST)
		del answer_check['csrfmiddlewaretoken']
		print(answer_check.values())
		answer=''
		for i in answer_check.values():
			print(i[0])
			if i[0]!=None and i[0]!='':
				answer+=i[0]
			else:
				answer+='-'
			print(answer)
		obj.answer=answer
		obj.save()
		return redirect('test_solve', id=id)

class NewView(TemplateView):
	template_name = "new/add_student_to_course.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["all_students"] =User.objects.filter(is_tutor=False) 
		return context
	


class TestListView(OwnerMixin,ListView):
	model=Test
	template_name='new/test_list.html'


class MyStudentView(OwnerCourseMixin, ListView):
	template_name = 'new/student_list.html'
	permission_required = 'courses.view_course'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		my_students=None
		my_courses=self.get_queryset()
		if my_courses.count() > 0:
			my_students=my_courses.first().students.all().distinct()
			for course in my_courses:
				my_students=(my_students | course.students.all().distinct()).distinct()
		context["all_students"] =my_students 
		return context
	

class CourseAddStudentView(LoginRequiredMixin,View):

	def get(self,request,slug,*args, **kwargs):
		course=get_object_or_404(Course,slug=slug,owner=request.user)
		students=User.objects.filter(is_tutor=False)
		context={'course':course,'all_students':students}
		return render(request,'new/add_student_to_course.html',context)
	
	
	def post(self,request,slug,*args,**kwargs):
		course=get_object_or_404(Course,slug=slug,owner=request.user)
		data=request.POST.getlist('student_id_list')
		n=0
		for i in data:
			print(i)
			try:
				student=get_object_or_404(User,id=i,is_tutor=False)
				if student not in course.students.all():
					course.students.add(student)
					n+=1
			except Exception as e:
				print(e)
				pass
				print(n)
		messages.add_message(request,messages.SUCCESS,f'{course} kursiga {n} ta o\'uvchi qo\'shildi.')
		return redirect('mystudents')


class CourseStudentDeleteView(View,PermissionRequiredMixin,LoginRequiredMixin):
	permission_required = 'courses.view_course'

	def get(self,request,slug,student_id):
		try:
			course=Course.objects.get(slug=slug,owner=request.user)
			student=User.objects.get(id=student_id)
			course.students.remove(student)
			messages.add_message(request,messages.SUCCESS,f'{course} kursidan {student} chiqarib yuborildi.')
		except:
			messages.add_message(request,messages.ERROR,f'O\'quvchini kursdan chiqarishda xatolik yuz berdi. Iltimos qayta urining.')
		return redirect('mystudents')



def get_course_sections(request):
	if request.is_ajax():
		course_id = request.GET.get('course', None)
		course=Course.objects.get(id=course_id)
		data=[]
		for section in course.section.all():
			print(section.id)
			data.append({'id':section.id,'title':section.title})
		print(data)
		return JsonResponse(data,safe=False)
	else:
		raise Http404