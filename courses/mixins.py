from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Course,Section,Lesson,Content,Subject


class MyCourseMixin(object):
    def get_object(self):
        qs = super().get_object()
        if qs.owner!=self.request.user:
            return None
        return qs

class OwnerMixin(object):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(owner=self.request.user)


class OwnerEditMixin(object):
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class OwnerCourseMixin(OwnerMixin,LoginRequiredMixin,PermissionRequiredMixin):
    model = Course
    fields = ['subject', 'title','overview','banner','video','about']
    success_url = reverse_lazy('manage_course_list')

    def get_context_data(self,*args, **kwargs):
        context=super().get_context_data(*args, **kwargs)
        context['subjects']=Subject.objects.all()
        return context

class OwnerCourseEditMixin(OwnerCourseMixin, OwnerEditMixin):
    template_name = 'new/tutor_course_edit.html'
    

class SectionOwnerMixin(object):
    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(section__owner=self.request.user)


class SectionOwnerCourseMixin(SectionOwnerMixin,LoginRequiredMixin,PermissionRequiredMixin):
    model = Section
    fields = ['subject', 'title','order',]
    success_url = reverse_lazy('manage_course_section_list')