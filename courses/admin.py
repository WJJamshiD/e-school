from django.contrib import admin
from .models import Subject,Course,Section,Lesson,Content,Test,TestResult,File
# Register your models here.


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display =['title','slug','created'] 
    # prepopulated_fields={'slug':('title',)}
    exclude=['slug']


class LessonInline(admin.StackedInline):
    model = Lesson
    prepopulated_fields={'slug':('title',)}
    extra=2


class SectionInline(admin.StackedInline):
    model = Section
    fields=['title','changeform_link']
    readonly_fields=['changeform_link',]
    extra=2


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display=['title','subject','created','owner','slug']
    list_filter=['created','subject','owner']
    search_fields=['title','overview'] 
    inlines=[SectionInline]
    prepopulated_fields={'slug':('title',)}


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display=['__str__','course','author_name']    
    inlines=[LessonInline]


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_display=['__str__','section','kurs_nomi']
    prepopulated_fields={'slug':('title',)}

@admin.register(Content)
class ContentAdmin(admin.ModelAdmin):
    list_display=['__str__','lesson','content_type']

@admin.register(File)
class ContentAdmin(admin.ModelAdmin):
    list_display=['title']

admin.site.register(Test)


@admin.register(TestResult)
class TestResultAdmin(admin.ModelAdmin):
    list_display=['student','test','result']
    exclude=['result']
