from django.shortcuts import render,get_object_or_404,redirect
from django.core.paginator import Paginator
from .models import Post
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse, HttpResponseRedirect, Http404
from .forms import PostForm
from django.contrib import messages 
from django.db.models import Q
from .utils import get_read_time
# Create your views here.

def post_list(request):
    if request.method=='GET':
        posts=Post.objects.all()
        # keyword=request.GET.get('qqq')
        # if keyword:
        #     query=posts.filter(Q(title__icontains=keyword)|Q(content__icontains=keyword))
        #     posts=query
        paginator = Paginator(posts, 4)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        latest=[]
        if (not page_number) or ( int(page_number) <=1):
            latest=page_obj[:3]

        form=PostForm()
        context={
            'posts':posts,
            'form':form,
            'page_obj':page_obj,
            'latest_posts':latest
            
        }
        return render(request,'blog/post_list.html',context)
    
        

def post_detail(request,slug=None):
    obj=get_object_or_404(Post,slug=slug)
    context={
        'post':obj
    }
    return render(request,'blog/post_detail.html',context)


def post_delete(request,slug=None):
    if request.method=='GET':
        if not request.user.is_tutor and not request.user.is_superuser:
            raise Http404
        obj=get_object_or_404(Post,slug=slug)
        #obj=Post.objects.get(slug=slug)
        obj.delete()
        messages.info(request,'Post muvaffaqiyatli o\'chirildi!')
        return redirect('blog')   

def post_edit(request,slug=None):
    post=get_object_or_404(Post,slug=slug)
    form=PostForm(request.POST or None,request.FILES or None,instance=post)
    if request.method=='GET':
        context={
            'form':form,
        }
        return render(request,'blog/post_form.html',context)
    if request.method=='POST':
        
        if form.is_valid():
            data=form.save(commit=False)
            data.save()
            messages.success(request,'Postga o\'zgartirishlar kiritildi!')
            return HttpResponseRedirect(data.get_absolute_url())
        else: 
            messages.error(request,'Xatolik yuz berdi, iltimos tekshirib qayta urining!')
            context={'form':form,
                    }
            return render(request, 'blog/post_form.html',context)


def post_create(request):
    if not (request.user.is_tutor or request.user.is_superuser):
        return redirect('blog')
    if request.method=='GET':
        form=PostForm()
        context={
            'form':form,
        }
        return render(request,'blog/post_form.html',context)
    if request.method=='POST':
        form=PostForm(request.POST or None,request.FILES or None)
        if form.is_valid():
            data=form.save(commit=False)
            data.user=request.user
            data.save()
            messages.success(request,'Post muvaffaqiyatli yaratildi!')
            return HttpResponseRedirect(data.get_absolute_url())
        else: 
            messages.error(request,'Xatolik yuz berdi, iltimos tekshirib qayta urining!')
            context={'form':form,
                    }
            return render(request, 'blog/post_form.html',context)

