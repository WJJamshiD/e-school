from django.contrib import admin
from django.urls import path,include,re_path
from . import views

urlpatterns = [
    re_path('^$',views.post_list, name='blog'),
    path('post/<str:slug>',views.post_detail, name='post_detail'),
    path('post/<str:slug>/edit',views.post_edit, name='edit'),
    path('post/<str:slug>/delete',views.post_delete, name='delete'),
    path('create/',views.post_create, name='post_create'),
]
