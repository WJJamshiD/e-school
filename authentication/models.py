from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin
from .managers import CustomUserManager
from django.db.models.signals import post_save,pre_save
from django.dispatch import receiver
from django.shortcuts import reverse
from django.contrib.auth.models import Permission
from django.utils.text import slugify 


class CustomUser(AbstractBaseUser,PermissionsMixin):
    username=models.CharField(max_length=200,unique=True, error_messages={'unique':"Ushbu login bilan boshqa o'quvchi ro'yxatga olingan."})
    phone=PhoneNumberField(null=True,blank=True,unique=True)
    # additional fields
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=50)
    email=models.EmailField( max_length=254,unique=False,null=True,blank=True)
    date_joined=models.DateTimeField(auto_now_add=True)
    last_login=models.DateTimeField(auto_now=True)
    # required cause using AbstractBaseUser
    is_active=models.BooleanField(default=True)
    is_staff=models.BooleanField(default=False)
    is_superuser=models.BooleanField(default=False)
    is_tutor=models.BooleanField(default=False,null=True,blank=True)

    USERNAME_FIELD='username'
    REQUIRED_FIELDS=['first_name','last_name']

    objects=CustomUserManager()
    class Meta:
        unique_together = [['first_name', 'last_name']]
    
    def __str__(self):
        return self.username

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'

    def get_absolute_url(self):
        return reverse('user_profile', kwargs={'pk':self.id})


    @property
    def min(self):
        if self.is_tutor:
            return None
        elif self.test_results.count() > 0:
            results=self.test_results.all()
            answers=[x.percentage for x in results]
            return min(answers)
        return None

    @property
    def max(self):
        if self.is_tutor:
            return None
        elif self.test_results.count() > 0:
            results=self.test_results.all()
            answers=[x.percentage for x in results]
            return max(answers)
        return None
    
    @property
    def percentage(self):
        if self.is_tutor:
            return None
        elif self.test_results.count() > 0:
            results=self.test_results.all()
            answers=[x.percentage for x in results]
            return int(sum(answers)/len(answers))
        return None
    
    @property
    def my_students(self):
        if self.is_tutor:
            my_courses=self.owned_courses.all()
            if my_courses.count() > 0:
                my_students=my_courses.first().students.all().distinct()
                for course in my_courses:
                    my_students=(my_students | course.students.all().distinct()).distinct()
                return my_students.count()
            return 0
        return None

@receiver(post_save, sender=CustomUser)
def user_post_save_receiver(sender,instance, **kwargs):
    profile,created=Profile.objects.get_or_create(user=instance)


  
def upload_path_profile(instan,file):
	return 'profiles/{0}/{1}'.format(slugify(instan.user.get_full_name()),file)

class Profile(models.Model):
    user=models.OneToOneField(CustomUser,on_delete=models.CASCADE, related_name='profile')
    second_name=models.CharField("Otchestvasi", max_length=50,blank=True,null=True)
    subject=models.ManyToManyField('courses.Subject', related_name='tutor',blank=True,null=True)
    img=models.ImageField(upload_to=upload_path_profile, height_field=None, width_field=None)
    parent_phone=PhoneNumberField(blank=True,null=True)
    #address
    street=models.CharField(max_length=120)
    region=models.CharField(max_length=120) 
    city=models.CharField(max_length=120)
    created=models.DateTimeField( auto_now=False, auto_now_add=True)
    updated=models.DateTimeField( auto_now=True, auto_now_add=False)
    about=models.TextField(max_length=5000)

    def __str__(self):
        return self.user.get_full_name()
    
    def get_address(self):
        adr=f'{self.city} {self.region} {self.street}'
        return adr
