from django import forms
from .models import CustomUser,Profile
from courses.models import Subject
from django.core.exceptions import NON_FIELD_ERRORS

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Parol', widget=forms.PasswordInput,)
    password2 = forms.CharField(label='Parolni tasdiqlash', widget=forms.PasswordInput)
    first_name=forms.CharField(label='Ism')
    last_name=forms.CharField(label='Familiya')
    username=forms.CharField(label='Login')
    is_tutor=forms.BooleanField(label="O'qituvchi",initial=False,required=False)

    class Meta:
        model = CustomUser
        fields = ('first_name','last_name','username','is_tutor')
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "Ushbu ism-familiyali o'quvchi ro'yxatdan o'tgan! Iltimos saytda ro'yxatdan o'tgan o'quvchilar orasidan izlab ko'ring!",
            }
        }


    def clean_password1(self):
        password1 = self.cleaned_data.get("password1")
    
        if len(password1)<7:
            raise forms.ValidationError("Parol juda qisqa, u kamida 7ta belgidan iborat bo'lishi lozim!")
        if password1.isdigit():
            raise forms.ValidationError("Parolda kamida bitta harf ishlatilishi kerak!")
        if password1.isalpha():
            raise forms.ValidationError("Parolda kamida bitta raqam ishlatilishi kerak!")      
            
        return password1

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Parollar mos kelmadi!")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class LoginForm(forms.Form):
    username=forms.CharField(required=True)
    password=forms.CharField()



CITIES=[('Toshkent','Toshkent'),
          ('Buxoro','Buxoro'),
          ('Samarqand','Samarqand'),
          ('Navoiy','Navoiy'),
          ('Qashqadaryo','Qashqadaryo'),
          ('Jizzax','Jizzax'),
          ('Andijon','Andijon'),
          ('Farg\'ona','Farg\'ona'),
          ('Namangan','Namangan'),
          ('Sirdaryo','Sirdaryo'),
          ('Xorazm','Xorazm'),
          ('Surxandaryo','Surxandaryo'),
          ('Qoraqalpog\'iston R','Qoraqalpog\'iston R'),
            ]


class TutorProfileForm(forms.ModelForm):
    city=forms.ChoiceField(choices=CITIES,label='')
    img = forms.FileField(widget=forms.FileInput(attrs={'placeholder': 'choose image..','id':'inputGroupFile01'}),label='')
    second_name=forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'Otangizning ismi.. (Otchestva)'}))
    about=forms.CharField(label='',widget=forms.Textarea(attrs={'placeholder': 'O\'zingiz haqida ma\'lumot bering..'}))
 
    class Meta:
        model = Profile
        exclude=['created','updated','user']


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Pochta manzilini kiriting.. ','required':'false'}),label='',required=False)
    phone = forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'ex: +998909863585'}))
    first_name=forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'Ismingizni kiriting..'}))
    last_name=forms.CharField(label='',widget=forms.TextInput(attrs={'placeholder': 'Familiyangizni kiriting..'}))
    
    class Meta:
        model=CustomUser
        fields=['first_name','last_name','email','phone']