from django.shortcuts import render,redirect
from .forms import LoginForm,UserCreationForm,UserUpdateForm,TutorProfileForm
from django.views.generic import View,TemplateView,CreateView,DetailView
from django.views.generic.base import TemplateResponseMixin
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.contrib import messages
from django.urls import reverse_lazy
from authentication.models import CustomUser
from courses.models import Subject,Course
from courses.mixins import OwnerMixin,MyCourseMixin
# Create your views here.

	
class LoginView(View):
	template_name = 'new/login.html'
	form_class=LoginForm
	
	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, {'form': self.form_class()})

	
	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			
			user_obj = authenticate(username=username, password=password)
			if user_obj is not None:
				if user_obj.is_active:
					login(request, user_obj)
					return redirect(user_obj.get_absolute_url())
				else:
					messages.add_message(
						request,
						messages.INFO,
						'Sizning akkauntingiz aktiv holda emas, iltimos adminstrator bilan bog\'laning!')
				# create 'Account disabled' template and render it 
					return redirect('login')
			else:
				messages.add_message(
						request,
						messages.ERROR,
						'Iltimos login va parolni to\'g\'ri kiriting!')
				print(messages)
				
		return render(request, self.template_name, {'form':form})





class ProfileUpdateView(TemplateView,LoginRequiredMixin):
	template_name='new/tutor_profile.html'
	user_form=UserUpdateForm
	profile_form=TutorProfileForm

	def get_context_data(self, *args,**kwargs):
		context = super().get_context_data(*args,**kwargs)
		context['subjects']=Subject.objects.all()
		context["user_form"] =self.user_form(instance=self.request.user) 
		context["profile_form"] =self.profile_form(instance=self.request.user.profile) 
		return context
	


	def post(self,request):
		subjects=Subject.objects.all()
		user_form=UserUpdateForm(instance=request.user,data=request.POST)
		profile_form=TutorProfileForm(instance=request.user.profile,
										data=request.POST,files=request.FILES)
	

		if user_form.is_valid() and profile_form.is_valid():
			user_form.save()
			profile_form.save()
			messages.add_message(
						request,
						messages.SUCCESS,
						'O\'zgarishlar muvaqqiyatli amalga oshirildi!')

		else:
			messages.add_message(
						request,
						messages.ERROR,
						'Xatolik yuz berdi iltimos tekshiring!')
			print(user_form.errors,profile_form.errors)
		
		return render(request,'new/tutor_profile.html',{'subjects':subjects,'user_form':user_form,'profile_form':profile_form})
		

	
	

class UserRegisterView(CreateView):
	template_name = 'new/register_student.html'
	form_class = UserCreationForm
	success_url = reverse_lazy('mystudents')
	
	def form_valid(self, form):
		result = super().form_valid(form)
		cd = form.cleaned_data
		user = authenticate(username=cd['username'], password=cd['password1'])
		return result



class UserProfileView(DetailView,LoginRequiredMixin,PermissionRequiredMixin):
	template_name = 'new/user_detail.html'
	model=CustomUser
	permission_required = 'custom_user.view_custom_user'


class AboutUsView(TemplateView):
	template_name='new/about_us.html'


class IndexView(TemplateView):
	template_name='new/homepage.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["courses"] =Course.objects.all() 
		context["subjects"] =Subject.objects.all() 
		return context
	

