from django.urls import path
from .views import LoginView,UserRegisterView,ProfileUpdateView,UserProfileView
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('login/',LoginView.as_view(),name='login'),
    path('logout/',LogoutView.as_view(),name='logout'),
    path('addstudent/', UserRegisterView.as_view(), name='add_student'),
    path('dashboard/', ProfileUpdateView.as_view(), name='tutor_dashboard'),
    path('<int:pk>',UserProfileView.as_view(), name='user_profile'),
]