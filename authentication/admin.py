from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, Profile
from .forms import UserCreationForm
# Register your models here.



class CustomUserAdmin(UserAdmin):
    add_form=UserCreationForm
    prepopulated_fields = {'username': ('first_name' , 'last_name', )}

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'last_name', 'username', 'password1', 'password2','is_tutor' ),
        }),
    )
    list_display=('username','get_full_name','phone','date_joined','last_login','is_tutor')
    list_filter=('is_tutor',)
    search_fields=('username','phone')
    readonly_fields=('date_joined','last_login')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name','last_name','email')}),
        ('Permissions', {'fields': ('is_tutor','is_active','is_staff','is_superuser','groups','user_permissions')}),
        ('Important dates',{'fields':('last_login','date_joined')})
    )

admin.site.register(CustomUser,CustomUserAdmin)

admin.site.register(Profile)

